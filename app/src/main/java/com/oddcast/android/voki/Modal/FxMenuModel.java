package com.oddcast.android.voki.Modal;

/**
 * Created by brst-pc20 on 5/13/16.
 */
public class FxMenuModel {

    String fxName, param;

    public String getFxName() {
        return fxName;
    }

    public void setFxName(String fxName) {
        this.fxName = fxName;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
