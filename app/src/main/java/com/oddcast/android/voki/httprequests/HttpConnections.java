package com.oddcast.android.voki.httprequests;

import android.util.Log;


import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by Vishal on 10/9/2015.
 */
public class HttpConnections {

    JSONObject jParentObject = null;
    String fxData;
    public static Connector connect = new Connector();
    static String response = "";
    static JSONObject retrieveJson;
    public static final String URL = "http://tiffin.tiffindaddy.com/index.php/api/";
//public static final String URL = "http://tiffindaddy.justdemo.org/index.php/api/";


   /* public String getSessionID() {
        String url = "http://vhss.oddcast.com/vhss_editors/getSession.php";
        HashMap<String, String> map = new HashMap<>();
        String str = connect.getStringData(map, url, "GET");
        Log.e("resp", str);
        return str;
    }
*/
    public String getSCID(String xml) {
        String url = "http://vhss.oddcast.com/vhss_editors/saveVokiScene.php";
        HashMap<String, String> map = new HashMap<>();
        map.put("saveXML", xml);
        map.put("dataStr", "");
        Log.d("xml", "" + xml);
        Log.d("map", "" + map);
        String str = connect.getStringData(map, url, "POST");
        Log.e("sciddd", "" + str);
        return str;
    }

    public String getTts(String engineId, String langId, String voiceId, String text, String extension,
                         String fxType, String fxLevel, String acc, String sessionId, String secretKey) {
        String url = "http://cache.oddcast.com/tts/gen.php?" + "EID=" + engineId + "&LID=" + langId + "&VID=" + voiceId + "&TXT=" + text + "&EXT=" + extension + "&FX_TYPE=" + fxType
                + "&FX_LEVEL=" + fxLevel + "&ACC=" + acc + "&SESSION=" + sessionId + "&CS=" + secretKey;

        Log.e("TtsUrl ", "" + url);
        HashMap<String, String> map = new HashMap<>();

        String str = connect.getStringData(map, url, "GET");
        Log.e("resp", "str : " + str);
        return str;
    }
}
