package com.oddcast.android.voki.custom_listeners;

/**
 * Created by brst-pc20 on 11/18/16.
 */

public class Manager {

    private static Manager manager;

    public static Manager getInstance() {
        if (manager == null)
            manager = new Manager();

        return manager;
    }

    public IonCallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(IonCallBack callBack) {
        this.callBack = callBack;
    }

    public IonCallBack callBack;
}
