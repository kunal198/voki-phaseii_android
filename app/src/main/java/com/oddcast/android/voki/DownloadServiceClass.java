package com.oddcast.android.voki;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.oddcast.android.voki.custom_listeners.IonCallBack;
import com.oddcast.android.voki.custom_listeners.Manager;

/**
 * Created by brst-pc20 on 11/18/16.
 */

public class DownloadServiceClass extends Service {
    public static ProgressDialog mProgressDialog;
    IonCallBack callBack;


    public static boolean isServiceRunning = false;


    @Override
    public void onCreate() {
        super.onCreate();
        callBack = Manager.getInstance().getCallBack();


        // Get the HandlerThread's Looper and use it for our Handler


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isServiceRunning = true;
        Log.d("Log_tag", "onStartCommand");

// execute this when the downloader must be fired

        DownloadTask downloadTask = new DownloadTask(DownloadServiceClass.this, callBack);
        downloadTask.execute("http://content.oddcast.com/ccs2/voki/mobile/android-data-v1.zip");


     /*   mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(false);
            }
        });*/

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
