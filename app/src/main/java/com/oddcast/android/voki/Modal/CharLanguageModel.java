package com.oddcast.android.voki.Modal;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 5/17/16.
 */
public class CharLanguageModel implements Serializable {

    String langId, charName, voiceId, engineId;

    public String getLangId() {
        return langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getVoiceId() {
        return voiceId;
    }

    public void setVoiceId(String voiceId) {
        this.voiceId = voiceId;
    }

    public String getEngineId() {
        return engineId;
    }

    public void setEngineId(String engineId) {
        this.engineId = engineId;
    }
}
