package com.oddcast.android.voki.Modal;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 5/19/16.
 */
public class AccessorySelectionModel implements Serializable {
    boolean facialHAir, hairs, mouth, bling, glasses, props, hat, costume;

    public boolean isFacialHAir() {
        return facialHAir;
    }

    public void setFacialHAir(boolean facialHAir) {
        this.facialHAir = facialHAir;
    }

    public boolean isHairs() {
        return hairs;
    }

    public void setHairs(boolean hairs) {
        this.hairs = hairs;
    }

    public boolean isMouth() {
        return mouth;
    }

    public void setMouth(boolean mouth) {
        this.mouth = mouth;
    }

    public boolean isBling() {
        return bling;
    }

    public void setBling(boolean bling) {
        this.bling = bling;
    }

    public boolean isGlasses() {
        return glasses;
    }

    public void setGlasses(boolean glasses) {
        this.glasses = glasses;
    }

    public boolean isProps() {
        return props;
    }

    public void setProps(boolean props) {
        this.props = props;
    }

    public boolean isHat() {
        return hat;
    }

    public void setHat(boolean hat) {
        this.hat = hat;
    }

    public boolean isCostume() {
        return costume;
    }

    public void setCostume(boolean costume) {
        this.costume = costume;
    }
}
