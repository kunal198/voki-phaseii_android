package com.oddcast.android.voki.Modal;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 5/11/16.
 */
public class ShowCategoryModel implements Serializable{

    String fileName , selection, selectImage, unselectImage;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getSelectImage() {
        return selectImage;
    }

    public void setSelectImage(String selectImage) {
        this.selectImage = selectImage;
    }

    public String getUnselectImage() {
        return unselectImage;
    }

    public void setUnselectImage(String unselectImage) {
        this.unselectImage = unselectImage;
    }
}
