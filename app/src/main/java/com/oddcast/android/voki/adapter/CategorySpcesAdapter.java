package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.CharacterListModel;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.utils.Constants;
import com.oddcast.android.voki.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * Created by brst-pc16 on 4/5/16.
 */
public class CategorySpcesAdapter extends BaseAdapter {

    Context mContext;
    //    ArrayList<HashMap<String, String>> specsCategory;
    ArrayList<CharacterListModel> charList;
    CharacterListener listener;

    //int position_char;

    ImageLoader imageLoader = ImageLoader.getInstance();

    private WeakHashMap<Integer, WeakReference<View>> hashMap = new WeakHashMap<Integer, WeakReference<View>>();


    public CategorySpcesAdapter(Context mContext, ArrayList<CharacterListModel> charList, CharacterListener listener) {
        this.mContext = mContext;
        this.charList = charList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return charList.size();
    }

    @Override
    public Object getItem(int position) {
        return charList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_cat_specs, null, true);
            holder = new ViewHolder();

            holder.im_icon = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (LinearLayout) convertView.findViewById(R.id.innerContainer);
            holder.smalldollarIV= (ImageView) convertView.findViewById(R.id.smalldollarIV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        /*if (position==0)
            holder.smalldollarIV.setVisibility(View.GONE);
        else
            holder.smalldollarIV.setVisibility(View.VISIBLE);*/
        hashMap.put(position, new WeakReference<View>(convertView));

        CharacterListModel charListModel = charList.get(position);
        try {
            if (charListModel.getGetAccsess()=="true")
                holder.smalldollarIV.setVisibility(View.GONE);
            else
                holder.smalldollarIV.setVisibility(View.VISIBLE);
            if (charListModel.getSelection().equals("true")) {
                String folderName = (String) charListModel.getSelectedFolder();
                String fileName = (String) charListModel.getFileName();//map.get("file_name");
               // "file:///mnt/sdcard.VokiData/assets/img/char/" + folderName + "/icons/over/btn.png";
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/" + fileName + "/thumb_128x128.png";
               // Glide.with(mContext).load(uri).into(holder.im_icon);
               // Picasso.with(mContext).load(uri).into(holder.im_icon);
                imageLoader.displayImage(uri, holder.im_icon);

                holder.container.setBackgroundResource(R.drawable.category_selectable_background);
            } else {
                String folderName = (String) charListModel.getSelectedFolder();
                String fileName = (String) charListModel.getFileName();//map.get("file_name");
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/" + fileName + "/thumb_128x128.png";
                imageLoader.displayImage(uri, holder.im_icon);
               // Glide.with(mContext).load(uri).into(holder.im_icon);
                //Picasso.with(mContext).load(uri).into(holder.im_icon);
                holder.container.setBackgroundResource(R.drawable.unselectable_background);
            }

//            }
        } catch (Exception e) {
            Log.e("newIoException", e.toString());
        }

        holder.container.setTag(position);
        holder.container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int pos = (Integer) v.getTag();
//                String[] fileName = specsCategory.get(pos).get("file_name").split("_");
                String[] fileName = charList.get(pos).getFileName().split("_");
                String file = fileName[0];
                Toast.makeText(mContext, file, Toast.LENGTH_SHORT).show();

                return true;
            }
        });


        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                MainActivity.isFirstTime = false;
                MainActivity.is_there_any_change_in_character=true;
                if (Constants.checkIfBuildVersionIsHigh())
                MainActivity.deleteVideoFile();
//                Log.e("sel_file",""+specsCategory.get(pos).get("file_name").split("_"));
                if (charList.size()>1)
                {
                    String[] fileName = charList.get(pos).getFileName().split("_");
                    String file = fileName[0];
                    String folder = charList.get(pos).getSelectedFolder();
//                Constants.PREVIOUS_CHARACTER_NAME = file;
                    Constants.NEW_CHARACTER_CATEGORY = folder;
                    Log.e("PREVIOUS_CHARACTER_NAME", "" + Constants.NEW_CHARACTER_CATEGORY);
                    showCharacter(pos);
                }

                //notifyDataSetChanged();


            }
        });
        return convertView;
    }

    static class ViewHolder {
        ImageView im_icon;
        LinearLayout container;
        ImageView smalldollarIV;
    }

    /**
     * Method for showing character from the list .............
     *
     * @param selectPos
     */
    public void showCharacter(final int selectPos) {
        // position_char = selectPos;
        if (MainActivity.webView!=null)
        MainActivity.webView.clearCache(true);
        MainActivity.isCharacterchangedagainForColorPallete = true;
        if (charList.get(selectPos).getGetAccsess()=="true")
        {
            MainActivity.dollarIV.setVisibility(View.GONE);
        }
        else{
            MainActivity.dollarIV.setVisibility(View.VISIBLE);
        }
        new AsyncTask<Void, Void, Void>() {

            String selected_category;
            String selectied_character;
            ArrayList<HashMap<String, String>> str_Builder;

            @Override
            protected void onCancelled() {
                super.onCancelled();
                cancel(true);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                try {
                    for (int i = 0; i < charList.size(); i++) {
                        if (i == selectPos) {
                            charList.get(selectPos).setSelection("true");

                        } else {
                            charList.get(i).setSelection("false");
                        }


                    }

                    selected_category = charList.get(selectPos).getSelectedFolder();//map.get("select_folder");
                    selectied_character = charList.get(selectPos).getFileName();//map.get("file_name");

                    Constants.TEMP_SAVED_CATEGORY = selected_category;
                    Constants.TEMP_SAVED_CHARACTER = selectied_character;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    str_Builder = Constants.readXml(mContext, selected_category, selectied_character, false);
                    Log.d("str_Builder>>>>>>",""+str_Builder);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try{
                    Constants.LAST_AUDIO_PLAYED = 0;
                    if (listener != null)
                        listener.onShowChracters(str_Builder,1);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
