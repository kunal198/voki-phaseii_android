package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.CharacterListModel;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 10/3/16.
 */
public class AllCharacterAdapter extends BaseAdapter {

   // LayoutInflater layoutInflater;
    ArrayList<CharacterListModel> charlist;
   Context mContext;
    ImageLoader imageLoader ;
    String getAccess="";
    CharacterListener listener;
    public AllCharacterAdapter(Context mContext,ArrayList<CharacterListModel> charList,CharacterListener listener)
    {
        this.mContext=mContext;
        this.charlist=charList;
    //    layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
        this.listener=listener;
    }

    @Override
    public int getCount() {
        return charlist.size();
    }

    @Override
    public Object getItem(int position) {
        return charlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.allcharactergrid, null, true);
            holder = new ViewHolder();
            holder.grid_image = (ImageView) convertView.findViewById(R.id.grid_image);
            holder.container= (LinearLayout) convertView.findViewById(R.id.container);
            holder.smalldollarIV= (ImageView) convertView.findViewById(R.id.small_dollar);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            CharacterListModel charListModel = charlist.get(position);
            getAccess=charListModel.getGetAccsess();
            if (charListModel.getGetAccsess()=="true")
                holder.smalldollarIV.setVisibility(View.GONE);
            else
                holder.smalldollarIV.setVisibility(View.VISIBLE);
            if (charListModel.getSelection().equals("true")) {
                String folderName = (String) charListModel.getSelectedFolder();
                String fileName = (String) charListModel.getFileName();//map.get("file_name");
                Log.e("folderName", charListModel.getSelectedFolder());
                Log.e("filename", charListModel.getFileName());
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/" + fileName + "/thumb_128x128.png";
               // Glide.with(mContext).load(Uri.parse(uri)).into(holder.grid_image);
                imageLoader.displayImage(uri, holder.grid_image);

                 holder.container.setBackgroundResource(R.drawable.category_selectable_background);
            } else {
                String folderName = (String) charListModel.getSelectedFolder();
                String fileName = (String) charListModel.getFileName();//map.get("file_name");
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/" + fileName + "/thumb_128x128.png";
                imageLoader.displayImage(uri, holder.grid_image);
              //  Glide.with(mContext).load(Uri.parse(uri)).into(holder.grid_image);
                   holder.container.setBackgroundResource(R.drawable.unselectable_background);
            }

            holder.grid_image.setTag(position);
            holder.grid_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos= (int) v.getTag();
                    if (charlist.get(pos).getGetAccsess()=="true")
                    {
                        MainActivity.dollarIV.setVisibility(View.GONE);
                    }
                    else{
                        MainActivity.dollarIV.setVisibility(View.VISIBLE);
                    }
                    MainActivity.allcharacterRL.setVisibility(View.GONE);
                    String[] fileName = charlist.get(pos).getFileName().split("_");
                    String file = fileName[0];
                    String folder = charlist.get(pos).getSelectedFolder();
//                Constants.PREVIOUS_CHARACTER_NAME = file;
                    showCharacter(pos);
                }
            });
//            }
        } catch (Exception e) {
            Log.e("newIoException", e.toString());
        }


        return convertView;
    }
    private class ViewHolder{
        ImageView grid_image;
        LinearLayout container;
        ImageView smalldollarIV;
    }
    public void showCharacter(final int selectPos) {
        // position_char = selectPos;
        MainActivity.webView.clearCache(true);
        MainActivity.isCharacterchangedagainForColorPallete = true;
        new AsyncTask<Void, Void, Void>() {

            String selected_category;
            String selectied_character;
            ArrayList<HashMap<String, String>> str_Builder;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                for (int i = 0; i < charlist.size(); i++) {
                    if (i == selectPos) {
                        charlist.get(selectPos).setSelection("true");

                    } else {
                        charlist.get(i).setSelection("false");
                    }


                }

                selected_category = charlist.get(selectPos).getSelectedFolder();//map.get("select_folder");
                selectied_character = charlist.get(selectPos).getFileName();//map.get("file_name");

                Constants.TEMP_SAVED_CATEGORY = selected_category;
                Constants.TEMP_SAVED_CHARACTER = selectied_character;
            }

            @Override
            protected Void doInBackground(Void... params) {
                str_Builder = Constants.readXml(mContext, selected_category, selectied_character, false);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                Constants.LAST_AUDIO_PLAYED = 0;
                if (listener != null)
                    listener.onShowChracters(str_Builder,2);

            }
        }.execute();
    }
}
