package com.oddcast.android.voki;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by brst-pc93 on 11/24/16.
 */

public class MySharedPreferences {
    private static MySharedPreferences instance = null;

    public final String PreferenceName = "MyPreference";

    public static MySharedPreferences getInstance() {
        if (instance == null) {
            instance = new MySharedPreferences();
        }
        return instance;
    }


    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
    }

    public void saveCharacterProductId(Context context, String character_product_id) {
        Set<String> setAll = getPreference(context).getStringSet("SAVED_CHARACTERS", null);
        if (setAll == null)
            setAll = new HashSet<>();

        Set<String> set = new HashSet<String>();
        set.add(character_product_id);
        setAll.addAll(set);

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putStringSet("SAVED_CHARACTERS", setAll);
        editor.apply();


    }

    public void freeCharacters(Context context) {

        Set<String> set = new HashSet<String>();
        set.add("Apollo_316");
        set.add("Aiko_217");
        set.add("Abysinian_2587");
        set.add("Ali G_2666");
        set.add("Abdul_3370");
        set.add("Falcomon_2534");
        set.add("Airedale Terrier_2443");
        set.add("Cheryl_1196");
        set.add("Evil Queen_27096");
        set.add("African Man_31238");
        set.add("Abe Lincoln_431");
        set.add("Alvar_2448");
        set.add("Alien Prime_90");
        set.add("Toon Bam_1310");
        set.add("Beach Dude_283");
        set.add("Angela Merkel_13791");
        set.add("Alien_481");
        set.add("Bashful_273");
        set.add("Cherie_2617");
        set.add("Abe Lincoln_431");
        set.add("Amber_1380");


        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putStringSet("FREE_CHARACTERS", set);
        editor.apply();


    }


    public void clearCharacterProductId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove("SAVED_CHARACTERS").apply();

    }


    public boolean isPurchased(Context context, String character_product_id) {

        Log.d("character_product_id", "" + character_product_id);
        boolean isPurchased = false;
        String char_prod_id = character_product_id.replace(" ", "");
        Set<String> setAll = getPreference(context).getStringSet("SAVED_CHARACTERS", null);
        if (setAll != null) {
            for (Iterator<String> character = setAll.iterator(); character.hasNext(); ) {

                String c = character.next();

                if (c.equalsIgnoreCase(character_product_id) || c.equalsIgnoreCase(char_prod_id) || c.equals("all_characters_buy")) {
                    isPurchased = true;
                    break;
                }
            }
        }
        return isPurchased;
    }

    public boolean isFreeCharacter(Context context, String character_product_id) {

        Log.d("character_product_id", "" + character_product_id);
        boolean isFree = false;
        Set<String> setAll = getPreference(context).getStringSet("FREE_CHARACTERS", null);
        if (setAll != null) {
            for (Iterator<String> character = setAll.iterator(); character.hasNext(); ) {

                String c = character.next();

                if (c.equalsIgnoreCase(character_product_id)) {
                    isFree = true;
                    break;
                }
            }
        }
        return isFree;
    }

    public Set<String> getAllPurchasedCharacter(Context context) {


        Set<String> setAll = getPreference(context).getStringSet("SAVED_CHARACTERS", null);

        return setAll;
    }

    public Set<String> getFreeCharacters(Context context) {


        Set<String> setAll = getPreference(context).getStringSet("FREE_CHARACTERS", null);

        return setAll;
    }

    public void saveSharingCount(Context context, int number) {


        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putInt("COUNT", number);
        editor.apply();


    }

    public int getSharingCount(Context context) {

        return getPreference(context).getInt("COUNT", 0);
    }

    public void setEligibleCount(Context context, int no) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putInt("E_COUNT", no);
        editor.apply();

    }

    public int getEligibleCount(Context context) {

        return getPreference(context).getInt("E_COUNT", 0);
    }

    public boolean getIsNeedToProcessReviewPopup(Context context) {

        return getPreference(context).getBoolean("TIMER", true);
    }

    public void setIsNeedToProcessReviewPopup(Context context, boolean timer) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putBoolean("TIMER", timer);
        editor.apply();

    }
}
