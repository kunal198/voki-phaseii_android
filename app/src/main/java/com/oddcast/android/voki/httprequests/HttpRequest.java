package com.oddcast.android.voki.httprequests;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Neeraj on 26-Sep-15.
 */
public class HttpRequest {

    private URL url;
    private HttpURLConnection con;
    private OutputStream os;
    //Can be instantiated with java.net.URL object, which checked for MalformedURLException by caller, so no need to declare here
    public HttpRequest(URL url){
        this.url=url;
    }
    //Can be instantiated with String representation of url, force caller to check for MalformedURLException which can be thrown by URL's constructor
    public HttpRequest(String url)throws MalformedURLException {
        this.url=new URL(url);
    }
    //used by all sending methods: send(), sendAndReadString(), sendAndReadJson() - to close open resources
    private void done(boolean isDelete)throws IOException {
        con.disconnect();
        if(isDelete)
        os.close();
    }
    /**
     * Sending connection and opening an output stream to server by pre-defined instance variable url
     *
//     * @param isPost boolean - indicates whether this request should be sent in POST method
     * @throws IOException - should be checked by caller
     * */
    private void prepareAll(boolean method, String data)throws IOException{
        con = (HttpURLConnection) url.openConnection();
        if(method) {
            Log.e("Inside Prepare","Method: post " + method);
            con.setRequestMethod(data);
        }
        con.setDoOutput(true);
        con.setDoInput(true);
        os = con.getOutputStream();
    }
    public HttpRequest prepare(String method) throws IOException{
        prepareAll(false, method);
        return this;
    }


    /**
     ** Sending connection and opening an output stream to server by pre-defined instance variable url
     * @throws IOException - should be checked by caller
     * @throws IOException
     */
    public void prepareDelete() throws IOException{
        con = (HttpURLConnection)url.openConnection();
//        con.setDoOutput(true);
//        con.setDoInput(true);
        con.setRequestMethod("DELETE");
        con.connect();
//        os = con.getOutputStream();

    }

    /**
     * prepare request in DELETE method
     * @return
     * @throws IOException
     */
    public HttpRequest deletePost() throws  IOException{
        prepareDelete();
        return  this;
    }

    /**
     * Writes query to open stream to server
     *
     * @param query String params in format of key1=v1&key2=v2 to open stream to server
     * @return HttpRequest this instance -> for chaining method @see line 22
     * @throws IOException - should be checked by caller
     * */
    public HttpRequest withData(String query) throws IOException{
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(query);
        writer.close();
        return this;
    }
    /**
     * Builds query on format of key1=v1&key2=v2 from given hashMap structure
     * for map: {name=Bubu, age=29} -> builds "name=Bubu&age=29"
     * for map: {Iam=Groot} -> builds "Iam=Groot"
     *
     * @param params HashMap consists of key-> value pairs to build query from
     * @return HttpRequest this instance -> for chaining method @see line 22
     * @throws IOException - should be checked by caller
     * */
    public HttpRequest withData(HashMap<String,String> params) throws IOException{
        String result="",and = "";
        for(Map.Entry<String, String> entry : params.entrySet()){
            if(entry.getKey().equals("file_upload"))Log.e("INSIDE WITH DATA","PDF FILE: "+entry.getValue());
            result+=and+entry.getKey()+"="+entry.getValue();//concats: key=value (for first param) OR &key=value(second and more)
            if(and.isEmpty())and="&";//& between params, except first so added after first concatenation
        }
        Log.e("INSIDE APPENDING","DATA: "+result);
        withData(result);
        return this;
    }
    //When caller only need to send, and don't need String response from server
    public boolean send() throws IOException{
        boolean status=con.getResponseCode()==HttpURLConnection.HTTP_OK;//Http OK == 200
        done(true);
        return status; //boolean return to indicate whether it successfully sent
    }
    /**
     * Sending request to the server and pass to caller String as it received in response from server
     *
     * @return String printed from server's response
     * @throws IOException - should be checked by caller
     * */
    public String sendAndReadString(boolean isDelete) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
        String line,response="";
        while ((line=br.readLine()) != null)response+=line;

        done(isDelete);
        return response;
    }
    //JSONObject representation of String response from server
    public JSONObject sendAndReadJSON(boolean isDelete) throws JSONException, IOException{
        String response = sendAndReadString(isDelete);
        Log.e("RESPONSE: ","DATA: "+response.toString().length());
        return new JSONObject(response);
    }
}
