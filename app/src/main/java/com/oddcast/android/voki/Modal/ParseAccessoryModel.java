package com.oddcast.android.voki.Modal;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 5/19/16.
 */
public class ParseAccessoryModel implements Serializable {

    String name, id, icon, selection;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }
}
