package com.oddcast.android.voki.SocialIntegrations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.oddcast.android.voki.custom_listeners.IonListners;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.oddcast.android.voki.MainActivity;


/**
 * Created by brst-pc20 on 6/24/16.
 */
public class TwitterIntegration extends Activity {
    int TWEETER_REQ_CODE = 11;
    private TwitterAuthClient client;
    IonListners listners;
    public  static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TwitterAuthConfig authConfig = new TwitterAuthConfig(VokiTeachApplication.TWITTER_KEY, VokiTeachApplication.TWITTER_SECRET);
//        Fabric.with(this, new Twitter(authConfig));
        context=this;

        boolean installed = appInstalledOrNot("com.twitter.android");
        if (installed) {
            client = new TwitterAuthClient();
            client.authorize(TwitterIntegration.this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> twitterSessionResult) {
                    try {
                        System.out.println("App is  installed on your phone");
                        TwitterSession session = twitterSessionResult.data;
                        // with your app's user model
                        String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";


                        Log.d("TwitterKitgetUserId", "Login with Twitter failure" + session.getUserName());
                        Intent intent = new TweetComposer.Builder(TwitterIntegration.this)
                                .text("My Voki has something to say\n\n" + MainActivity.final_url_for_sharing + "\n\nTo create your own Voki go to: www.voki.com")
                                .image(Uri.fromFile(MainActivity.photo))
                                .createIntent();
                        startActivityForResult(intent, TWEETER_REQ_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // finish();

                    //  Toast.makeText(TwitterIntegration.this, "Tweet posted", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void failure(TwitterException e) {
                    Log.d("TwitterKit", "Login with Twitter failure", e);
                    finish();
                }
            });

        } else {
            finish();

            Toast.makeText(TwitterIntegration.this, " Please Install the Twitter App on your phone before sharing.", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Make sure that the loginButton hears the result from any
        // Activity that it triggered.
//     client.onActivityResult(requestCode, resultCode, data);
        if (requestCode != TWEETER_REQ_CODE) {
            client.onActivityResult(requestCode, resultCode, data);
        }
        if (data != null && requestCode == TWEETER_REQ_CODE && resultCode == RESULT_OK) {

            Toast.makeText(this, "Your Voki has been shared", Toast.LENGTH_SHORT).show();

            Log.d("postdelay", "postdelay");
            setResult(78,getIntent());
            finish();

        }

        Log.d("twitter", "enter" + RESULT_OK);
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

}



