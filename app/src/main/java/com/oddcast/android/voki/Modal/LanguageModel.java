package com.oddcast.android.voki.Modal;

import java.io.Serializable;

/**
 * Created by brst-pc20 on 5/17/16.
 */
public class LanguageModel implements Serializable {

    String languageName, languageId;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }
}
