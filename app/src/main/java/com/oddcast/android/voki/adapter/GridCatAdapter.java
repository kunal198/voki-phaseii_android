package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.oddcast.android.voki.utils.Constants;
import com.oddcast.android.voki.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc16 on 5/4/16.
 */
public class GridCatAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<HashMap<String, String>> optionList;
    ViewHolder holder;

    public GridCatAdapter(Context mContext, ArrayList<HashMap<String, String>> optionList) {
        this.mContext = mContext;
        this.optionList = optionList;
    }

    @Override
    public int getCount() {
        return optionList.size();
    }

    @Override
    public Object getItem(int position) {
        return optionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_characters, null, true);

            holder = new ViewHolder();

            holder.im_characters = (ImageView) convertView.findViewById(R.id.im_character);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.innerContainer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap map = optionList.get(position);
        try {
            if (map.get(Constants.isSelectedKey).equals("true")) {
                String folderName = (String) map.get("file_name");
                Bitmap bmp = null;
                bmp = BitmapFactory.decodeStream(mContext.getAssets().open("img/ui/icons/over/" + folderName));
                holder.im_characters.setImageBitmap(bmp);

            } else {
                String folderName = (String) map.get("file_name");
                Bitmap bmp = null;
                bmp = BitmapFactory.decodeStream(mContext.getAssets().open("img/ui/icons/base/" + folderName));
                holder.im_characters.setImageBitmap(bmp);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(map.get("isEnabled") != null ) {
            if (map.get("isEnabled").equals("true")) {
                holder.container.setBackgroundColor(Color.TRANSPARENT);
            } else {
                holder.container.setBackgroundColor(Color.parseColor("#66000000"));
            }
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView im_characters;
        RelativeLayout container;
    }
}
