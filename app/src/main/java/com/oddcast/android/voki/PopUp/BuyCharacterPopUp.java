package com.oddcast.android.voki.PopUp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.app.Activity;
import android.widget.Toast;

import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.MySharedPreferences;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.ViewUtill;
import com.oddcast.android.voki.util.IabHelper;
import com.oddcast.android.voki.util.IabResult;
import com.oddcast.android.voki.util.Purchase;
import com.oddcast.android.voki.utils.Constants;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 8/20/16.
 */
public class BuyCharacterPopUp extends Dialog implements View.OnClickListener, IabHelper.OnIabPurchaseFinishedListener {

    private static Activity mactivity;
    private ImageView ivCancel;
    PopupWindow pwindo;
    IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener;
    TextView buyOneAvatarTV, buyAllAvatarsTV, restore_purchasesTV, unlockallavatarTV;

    public BuyCharacterPopUp(Activity activity) {
        super(activity);
        mactivity = activity;
        onIabPurchaseFinishedListener = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.unlock_voki_popup);
        buyOneAvatarTV = (TextView) findViewById(R.id.buyOneAvatarTV);
        unlockallavatarTV = (TextView) findViewById(R.id.unlockallavatarTV);

        buyAllAvatarsTV = (TextView) findViewById(R.id.buyAllAvatarsTV);
        ivCancel = (ImageView) findViewById(R.id.iv_cancel);
        restore_purchasesTV = (TextView) findViewById(R.id.restore_purchasesTV);
        ivCancel.setOnClickListener(this);
        buyOneAvatarTV.setOnClickListener(this);
        buyAllAvatarsTV.setOnClickListener(this);
        restore_purchasesTV.setOnClickListener(this);
        unlockallavatarTV.setOnClickListener(this);


        String sss = mactivity.getResources().getString(R.string.unlock_hunderds_of_voki);
        int pos = sss.indexOf("learn more");
        Log.d("index_pos", "" + pos);
        SpannableString ss = new SpannableString(sss);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Log.d("index_pos", "onClick");
                // startActivity(new Intent(MyActivity.this, NextActivity.class));
                // initiateWhyLoginPopupWindow();
                initiateWhyBuyAllCharWindow();
                dismiss();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, pos, pos + 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        unlockallavatarTV.setText(ss);
        unlockallavatarTV.setTextColor(Color.WHITE);
        unlockallavatarTV.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.iv_cancel:
                dismiss();
                break;

            case R.id.buyOneAvatarTV:
                try {
                    dismiss();
                    Constants.buySingleCharacter(mactivity, Constants.TEMP_SAVED_CHARACTER, onIabPurchaseFinishedListener);
                } catch (Exception e) {
                    Toast.makeText(mactivity, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                break;

            case R.id.buyAllAvatarsTV:
                try {
                    dismiss();
                    Constants.buyAllCharacter(mactivity, onIabPurchaseFinishedListener);
                } catch (Exception e) {
                    Toast.makeText(mactivity, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            case R.id.restore_purchasesTV:
                try {
                    if (ViewUtill.haveNetworkConnection(mactivity)) {
                        restoreDataOfPurchasedCharacter();
                        dismiss();
                    } else {
                        Constants.internetPopup(mactivity);
                    }
                } catch (Exception e) {
                    Toast.makeText(mactivity, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                // if continuationToken != null, call getPurchases again
                // and pass in the token to retrieve more items


                break;

            default:
                break;
        }

    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        Log.d("Log_tag", "Purchase finished: " + result.getResponse() + ", purchase: " + info);

        // if we were disposed of in the meantime, quit.

        if (result.isFailure()) {
            if (result.getResponse() == 7) {
                dismiss();
                AlreadypurchasedPopUp alreadypurchasedPopUp = new AlreadypurchasedPopUp(mactivity, "");
                alreadypurchasedPopUp.show();
                alreadypurchasedPopUp.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 60 / 100,
                        mactivity.getResources().getDisplayMetrics().heightPixels * 65 / 100);
                alreadypurchasedPopUp.setCancelable(false);

            } else if (result.getResponse() == -1005) {
                ((MainActivity) mactivity).initiatePopupWindow("Purchase Not Successful", "");
            }
            Log.d("Log_tag", "Purchase failure.");
            return;
        }

        if (result.isSuccess()) {
            MySharedPreferences.getInstance().saveCharacterProductId(mactivity, Constants.TEMP_SAVED_CHARACTER);
            MainActivity.dollarIV.setVisibility(View.GONE);
            if (MainActivity.catSpecsAdapter != null)
                MainActivity.catSpecsAdapter.notifyDataSetChanged();
            dismiss();
        }


        Log.d("Log_tag", "Purchase successful.");
    }

    /*
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            Log.d("Log_tag", "onActivityResult(" + requestCode + "," + resultCode + "," + data);
            if (Constants.mHelper == null) return;

            // Pass on the activity result to the helper for handling
            if (!Constants.mHelper.handleActivityResult(requestCode, resultCode, data)) {
                // not handled, so handle it ourselves (here's where you'd
                // perform any handling of activity results not related to in-app
                // billing...
                super.onActivityResult(requestCode, resultCode, data);
            }
            else {
                Log.d(TAG, "onActivityResult handled by IABUtil.");
            }
        }*/
    public void initiateWhyBuyAllCharWindow() {
        //    try {
// We need to get the instance of the LayoutInflater
          /*  LayoutInflater inflater = (LayoutInflater) mactivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.why_buy_all_characters, null, true);


            float width_ = MainActivity.st_pos_width / 4 + MainActivity.st_pos_width;
            int final_height = (int) MainActivity.screenHeight;
            int final_width = (int) width_;
*/
        BuyAllChar buyAllChar = new BuyAllChar(mactivity);
        buyAllChar.show();
        buyAllChar.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 70 / 100,
                mactivity.getResources().getDisplayMetrics().heightPixels * 100 / 100);

        buyAllChar.setCancelable(false);


           /* final Dialog dialog = new Dialog(mactivity);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.why_buy_all_characters);
            ImageView iv_cancel = (ImageView) findViewById(R.id.iv_cancel_login);
            TextView buyAllAvatarsTV = (TextView) findViewById(R.id.buyAllAvatarsTV);
            TextView whyloginDetail1TV = (TextView) findViewById(R.id.whyloginDetail1TV);
            String text=mactivity.getResources().getString(R.string.buy_all_voki_char).toString();
            whyloginDetail1TV.setText(text+" "+MainActivity.link3);

            iv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("button", "clicked");
                   dialog.dismiss();
                    BuyCharacterPopUp buyCharacterPopUpp = new BuyCharacterPopUp(mactivity);
                    buyCharacterPopUpp.show();
                    buyCharacterPopUpp.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 70 / 100,
                            mactivity.getResources().getDisplayMetrics().heightPixels * 100 / 100);

                }
            });
            buyAllAvatarsTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("button", "clickeddd");
                    Constants.buyAllCharacter(mactivity, onIabPurchaseFinishedListener);

                }
            });

            dialog.show();

*/


        /*} catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    public static void restoreDataOfPurchasedCharacter() {
        Bundle ownedItems = null;
        try {
            ownedItems = MainActivity.mService.getPurchases(3, "com.oddcast.android.voki", "inapp", null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d("Log_tag----------", "" + ownedItems);
        int response = ownedItems.getInt("RESPONSE_CODE");
        Log.d("Log_tag-re", "" + response);
        if (response == 0) {
            ArrayList<String> ownedSkus =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
            ArrayList<String> purchaseDataList =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
            ArrayList<String> signatureList =
                    ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
            String continuationToken =
                    ownedItems.getString("INAPP_CONTINUATION_TOKEN");
            Log.d("Log_tag----", "purchaseDataList" + purchaseDataList.size());
            Log.d("Log_tag----", "purchaseDataList" + ownedSkus.size());
            Log.d("Log_tag----", "purchaseDataList" + signatureList.size());
            MySharedPreferences.getInstance().clearCharacterProductId(mactivity);
            if (purchaseDataList.size() == 0) {
                AlreadypurchasedPopUp alreadypurchasedPopUp = new AlreadypurchasedPopUp(mactivity, "You have not purchased\n     any character yet.");
                alreadypurchasedPopUp.show();
                alreadypurchasedPopUp.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 60 / 100,
                        mactivity.getResources().getDisplayMetrics().heightPixels * 65 / 100);

                alreadypurchasedPopUp.setCancelable(false);
            } else {
                for (int i = 0; i < purchaseDataList.size(); ++i) {
                    Log.d("Log_tag----", "enter");
                    String purchaseData = purchaseDataList.get(i);
                    String signature = signatureList.get(i);
                    String sku = ownedSkus.get(i);
                    MySharedPreferences.getInstance().saveCharacterProductId(mactivity, sku);
                    Log.d("Log_tag-1", "" + purchaseData);
                    Log.d("Log_tag-2", "" + signature);
                    Log.d("Log_tag-3", "" + sku);
                    // do something with this purchase information
                    // e.g. display the updated list of products owned by user


                }
                AlreadypurchasedPopUp alreadypurchasedPopUp = new AlreadypurchasedPopUp(mactivity, "Your purchases have\n      been restored.");
                alreadypurchasedPopUp.show();
                alreadypurchasedPopUp.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 60 / 100,
                        mactivity.getResources().getDisplayMetrics().heightPixels * 65 / 100);

                alreadypurchasedPopUp.setCancelable(false);
            }

            boolean isPurchasedChar = MySharedPreferences.getInstance().isPurchased(mactivity, Constants.TEMP_SAVED_CHARACTER);
            //if (isPurchasedChar?MainActivity.dollarIV.setVisibility(View.GONE):MainActivity.dollarIV.setVisibility(View.VISIBLE))
            MainActivity.dollarIV.setVisibility(isPurchasedChar ? View.GONE : View.VISIBLE);


        }
    }

    private class BuyAllChar extends Dialog {

        public BuyAllChar(Context context) {
            super(context);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            setContentView(R.layout.why_buy_all_characters);
            ImageView iv_cancel = (ImageView) findViewById(R.id.iv_cancel_login);
            TextView buyAllAvatarsTV = (TextView) findViewById(R.id.buyAllAvatarsTV);
            TextView whyloginDetail1TV = (TextView) findViewById(R.id.whyloginDetail1TV);
            String text = mactivity.getResources().getString(R.string.buy_all_voki_char).toString();
            whyloginDetail1TV.setText(text + " " + MainActivity.link3);

            iv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("button", "clicked");
                    dismiss();
                    BuyCharacterPopUp buyCharacterPopUpp = new BuyCharacterPopUp(mactivity);
                    buyCharacterPopUpp.show();
                    buyCharacterPopUpp.getWindow().setLayout(mactivity.getResources().getDisplayMetrics().widthPixels * 75 / 100,
                            mactivity.getResources().getDisplayMetrics().heightPixels * 100 / 100);

                }
            });
            buyAllAvatarsTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("button", "clickeddd");
                    Constants.buyAllCharacter(mactivity, onIabPurchaseFinishedListener);

                }
            });
        }


    }


}