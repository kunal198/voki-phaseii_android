package com.oddcast.android.voki.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.HashMap;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.Helper;
import com.oddcast.android.voki.R;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by brst-pc20 on 11/30/16.
 */

public class PurchasedCharacterAdapter extends BaseAdapter {
    Set<String> setAll;
    LayoutInflater layoutInflater;
    ArrayList<String> charlist;
    Context mContext;
    ImageLoader imageLoader;
    Helper helper = new Helper();
    HashMap<String, String> character_path;

    public PurchasedCharacterAdapter(Context mContext, Set<String> setAll) {
        this.mContext = mContext;

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
        if (setAll != null)
            charlist = new ArrayList<>(setAll);

        character_path = helper.pathsOfPurchasedCharacters();
    }

    @Override
    public int getCount() {
        Log.d("getcount", "" + charlist.size());
        return charlist.size();
    }

    @Override
    public Object getItem(int position) {
        return charlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PurchasedCharacterAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.purchased_char_grid_row, null, true);
            holder = new PurchasedCharacterAdapter.ViewHolder();
            holder.purchased_char = (ImageView) convertView.findViewById(R.id.im_prchasedCharacter);

            convertView.setTag(holder);

        } else {
            holder = (PurchasedCharacterAdapter.ViewHolder) convertView.getTag();
        }

        try {
            Log.e("charList", "" + charlist.get(position));
            String character_name = charlist.get(position).replace(" ","").toLowerCase();
            if (!character_name.equals("all_characters_buy"))
            {
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + character_path.get(character_name) + "/thumb_128x128.png";
                Glide.with(mContext).load(uri).into(holder.purchased_char);
               // imageLoader.displayImage(uri, holder.purchased_char);
            }
            // Glide.with(mContext).load(Uri.parse(uri)).into(holder.purchased_char);



//            }
        } catch (Exception e) {
            Log.e("newIoException", e.toString());
        }


        return convertView;
    }

    private class ViewHolder {
        ImageView purchased_char;

    }
}
    




