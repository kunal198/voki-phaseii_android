package com.oddcast.android.voki;

import android.os.AsyncTask;
import android.util.Log;

import com.oddcast.android.voki.custom_listeners.IonCallBack;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.thefinestartist.utils.content.ContextUtil.sendBroadcast;

/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 *
 * @author www.codejava.net
 */
public class UnzipUtility {

    /**
     * Size of the buffer to read/write data
     */
    private static final int BUFFER_SIZE = 1024 * 8;
    private IonCallBack callBack;

    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     *
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public void unzip(final String zipFilePath, final String destDirectory, final IonCallBack callBack) throws IOException {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                File destDir = new File(destDirectory);
                if (!destDir.exists()) {
                    destDir.mkdir();
                }
            /*    File file = new File(zipFilePath);
                Log.d("zipfilepath", "--" + file.exists());*/
                ZipInputStream zipIn = null;
                try {
                    zipIn = new ZipInputStream(new FileInputStream(zipFilePath));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                ZipEntry entry = null;
                try {
                    entry = zipIn.getNextEntry();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // iterates over entries in the zip file
                while (entry != null) {
                    String filePath = destDirectory + File.separator + entry.getName();
                    if (!entry.isDirectory()) {
                        // if the entry is a file, extracts it
                        try {
                            extractFile(zipIn, filePath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        // if the entry is a directory, make the directory
                        File dir = new File(filePath);
                        dir.mkdir();
                    }
                    try {
                        zipIn.closeEntry();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        entry = zipIn.getNextEntry();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("dismiss", "dismiss");
                try {
                    zipIn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (callBack != null)
                    callBack.onSuccess(2);
            }
        }.execute();


        /*Intent intent = new Intent("com.tutorialspoint.CUSTOM_INTENT");
        sendBroadcast(intent);*/
    }

    /**
     * Extracts a zip entry (file entry)
     *
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }

        bos.close();
    }
}
