package com.oddcast.android.voki.custom_listeners;

/**
 * Created by brst-pc20 on 12/12/16.
 */

public interface StoppingMediaRecorder {



    public void playAudio();
    public void stopMediaRecorder();
}
