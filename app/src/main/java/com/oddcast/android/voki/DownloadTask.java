package com.oddcast.android.voki;

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.oddcast.android.voki.custom_listeners.IonCallBack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by brst-pc20 on 11/17/16.
 */
public class DownloadTask extends AsyncTask<String, Integer, String> {
    int id = 1;
    int lastProgress = 0;

    private Context context;
    private PowerManager.WakeLock mWakeLock;

    public static NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private IonCallBack callBack;

    public DownloadTask(Context context, IonCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream("/sdcard/.VokiZip/voki.zip");

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();



        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("Files Download")
                .setContentText("Download in progress")
                .setSmallIcon(R.mipmap.logo);
        String dir_path = Environment.getExternalStorageDirectory().getPath() + "/.VokiZip/";
        if (!dir_exists(dir_path)){
            File directory = new File(dir_path);
            directory.mkdirs();
        }


    }

    @Override
    protected void onProgressUpdate(final Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
      /*  MainActivity.mProgressDialog.setIndeterminate(false);
        MainActivity.mProgressDialog.setMax(100);
        MainActivity.mProgressDialog.setProgress(progress[0]);*/
        Log.d("progress", "" + progress);
        mBuilder.setProgress(100, progress[0], false);
// Issues the notification

        if (lastProgress != progress[0]) {
            lastProgress = progress[0];
            mBuilder.setNumber(progress[0]);
            mNotifyManager.notify(id, mBuilder.build());
        }
        if (MainActivity.isNeedTodestroyPopUp)
        {
            if (progress[0]<100)
                MainActivity.percentage_progress.setText(String.valueOf(progress[0])+"%");
            else
                MainActivity.percentage_progress.setText("DONE");
            MainActivity.circularProgressBar.setProgressWithAnimation(progress[0], 2500);
        }

        if (progress[0] == 100) {
            mNotifyManager.cancel(id);
            MainActivity.progress_bar_messagee.setText("Unzipping files.Please wait..");
        }

    }

    @Override
    protected void onPostExecute(String result) {
        mWakeLock.release();
        mBuilder.setContentText("Download complete");
        if (result != null)
            Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
        String zipFilePath = "/sdcard/.VokiZip/voki.zip";
        String destDirectory = "/sdcard/.VokiData";
        if (callBack != null&&MainActivity.isNeedTodestroyPopUp)
            callBack.onSuccess(1);
        // Removes the progress bar
        UnzipUtility unzipper = new UnzipUtility();
        try {
            unzipper.unzip(zipFilePath, destDirectory, callBack);
        } catch (Exception ex) {
            // some errors occurred
            ex.printStackTrace();
        }
        if (callBack != null)
            callBack.onSuccess(1);

    }

    public boolean dir_exists(String dir_path)
    {
        boolean ret = false;
        File dir = new File(dir_path);
        if(dir.exists() && dir.isDirectory())
            ret = true;
        return ret;
    }
}




