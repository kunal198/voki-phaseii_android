package com.oddcast.android.voki.Modal;

/**
 * Created by brst-pc20 on 5/11/16.
 */
public class CharacterListModel {

    String selectedFolder, fileName, thumbnailImage, selection,getAccsess;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSelectedFolder() {
        return selectedFolder;
    }

    public void setSelectedFolder(String selectedFolder) {
        this.selectedFolder = selectedFolder;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getSelection() {
        return selection;
    }

    public String getGetAccsess() {
        return getAccsess;
    }

    public void setGetAccsess(String getAccsess) {
        this.getAccsess = getAccsess;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }
}
