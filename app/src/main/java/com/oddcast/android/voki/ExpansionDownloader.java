package com.oddcast.android.voki;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * Created by brst-pc20 on 11/16/16.
 */

public class ExpansionDownloader extends DownloaderService{

    public static final String BASE64_PUBLIC_KEY ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuN2R9hSQ5MAqVVXY12SMcxR/kKHgoIJ5E1SHqCjwYdPS4EhF01C45Rv0oXviSOfJeeZ+YcO1fyxfWMI2QvO7KYiNKnGOPkjiCfe8lAHE7fN1eFql5Txf0zMrWEtoa2t2PUYRjbHDYg+sW4t3gIgis49kl/Vhr+/HtJ4uph7jQ8jvlYYt3HaT3itt+S7tczKNcUYhZVfz0BQ59WRmRbXj2z4XET5ex/YHRMXvXQeI0jhANzw8T8jhUyPbqoDG5JIlIzFQHF6qvWmzvHB1ttLtZtwr9hqzdXoZ47J571EbSB1jcqnrYm0OZ8baAiET4ldwimtIvr9DYZTdUh56EcEIuwIDAQAB";
    public static final byte[] SALT = new byte[] { 1, 42, -12, -1, 54, 98,
            -100, -12, 43, 2, -8, -4, 9, 5, -106, -107, -33, 45, -1, 84
    };

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return AlarmReceiver.class.getName();

    }
}
