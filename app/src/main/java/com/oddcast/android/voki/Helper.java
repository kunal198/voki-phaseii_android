package com.oddcast.android.voki;

import java.util.HashMap;

public class Helper {


    public HashMap<String, String> pathsOfPurchasedCharacters() {


        HashMap<String, String> map = new HashMap<>();
        map.put("johnkerry_596", "POLITICS/John Kerry_596");
        map.put("davidcameron_13753", "POLITICS/David Cameron_13753");
        map.put("bat_790", "Monsters/Bat_790");
        map.put("indianwoman_28332", "Folk/Indian Woman_28332");
        map.put("earlbear_672", "Animals/Earl Bear_672");
        map.put("winston_1051", "Dogs/Winston_1051");
        map.put("erik_2261", "3D/Erik_2261");
        map.put("poodle_2554", "Dogs/Poodle_2554");
        map.put("alvina_2444", "Holiday/Alvina_2444");
        map.put("boxer_2467", "Dogs/Boxer_2467");
        map.put("alvar_2448", "Holiday/Alvar_2448");
        map.put("switch_232", "Randoms/Switch_232");
        map.put("nova_1295", "Randoms/Nova_1295");
        map.put("obake_1446", "Randoms/Obake_1446");
        map.put("toontam_1283", "Oddballs/Toon Tam_1283");
        map.put("fairy_26969", "Fantasy/Fairy_26969");
        map.put("prince_26346", "Fantasy/Prince_26346");
        map.put("sumowrestler_270", "Randoms/Sumo Wrestler_270");
        map.put("wizard_798", "Monsters/Wizard_798");
        map.put("pixie_411", "Cats/Pixie_411");
        map.put("littlebritain_2386", "Dogs/Little Britain_2386");
        map.put("happyface_237", "Smilies/Happy Face_237");
        map.put("tooncam_1304", "Oddballs/Toon Cam_1304");
        map.put("daisy_682", "Randoms/Daisy_682");
        map.put("turkey_110", "Holiday/Turkey_110");
        map.put("chinesewoman_31323", "Folk/Chinese Woman_31323");
        map.put("dragon_94", "Monsters/Dragon_94");
        map.put("princess_25515", "Fantasy/Princess_25515");
        map.put("harry_228", "Animals/Harry_228");
        map.put("miniatureschnauzer_2689", "Dogs/Miniature Schnauzer_2689");
        map.put("brenda_108", "Classic/Brenda_108");
        map.put("witch_799", "Monsters/Witch_799");
        map.put("santa_109", "Holiday/Santa_109");
        map.put("easteregg_184", "Holiday/Easter Egg_184");
        map.put("flower_504", "Randoms/Flower_504");
        map.put("ghost_793", "Randoms/Ghost_793");
        map.put("unclesam_679", "World/Uncle Sam_679");
        map.put("lilly_1302", "3D/Lilly_1302");
        map.put("melinda_689", "Toons/Melinda_689");
        map.put("happyheart_154", "Holiday/Happy Heart_154");
        map.put("gnome_27189", "Fantasy/Gnome_27189");
        map.put("tonyblair_296", "POLITICS/Tony Blair_296");
        map.put("buddha_436", "World/Buddha_436");
        map.put("jaws_92", "Animals/Jaws_92");
        map.put("juliette_76", "3D/Juliette_76");
        map.put("germanshepherd_2205", "Dogs/German Shepherd_2205");
        map.put("nicolassarkozy_13788", "POLITICS/Nicolas Sarkozy_13788");
        map.put("bryan_103", "Classic/Bryan_103");
        map.put("pilgrimman_941", "Holiday/Pilgrim Man_941");
        map.put("schipperke_2536", "Dogs/Schipperke_2536");
        map.put("pink_891", "Edgy/Pink_891");
        map.put("sakura_486", "Anime/Sakura_486");
        map.put("johnsonsnail_678", "Animals/Johnson Snail_678");
        map.put("junichirokoizumi_301", "POLITICS/Junichiro Koizumi_301");
        map.put("bryson_2931", "Classic/Bryson_2931");
        map.put("keenan_2544", "Digimon/Keenan_2544");
        map.put("rudolph_956", "Holiday/Rudolph_956");
        map.put("ginger_2014", "3D/Ginger_2014");
        map.put("werewolf_98", "Monsters/Werewolf_98");
        map.put("mittromney_788", "POLITICS/Mitt Romney_788");
        map.put("pumpkin_100", "Holiday/Pumpkin_100");
        map.put("electricsheep_680", "Animals/Electric Sheep_680");
        map.put("kaya_2402", "Randoms/Kaya_2402");
        map.put("hollydae_2209", "Randoms/Holly Dae_2209");
        map.put("llilybabydoll_492", "Randoms/Llily Babydoll_492");
        map.put("skully_2166", "Randoms/Skully_2166");
        map.put("juliagillard_24393", "POLITICS/Julia Gillard_24393");
        map.put("chineseman_31288", "Folk/Chinese Man_31288");
        map.put("bashful_273", "Smilies/Bashful_273");
        map.put("alienprime_90", "Monsters/Alien Prime_90");
        map.put("pirate_96", "Monsters/Pirate_96");
        map.put("santatoo_382", "Holiday/Santa Too_382");
        map.put("airedaleterrier_2443", "Dogs/Airedale Terrier_2443");
        map.put("kitcat_225", "Cats/Kit Cat_225");
        map.put("toonsam_1214", "Oddballs/Toon Sam_1214");
        map.put("jester_27061", "Fantasy/Jester_27061");
        map.put("layne_763", "Edgy/Layne_763");
        map.put("mahatmagandhi_433", "World/Mahatma Gandhi_433");
        map.put("mermaid_27370", "Fantasy/Mermaid_27370");
        map.put("looneylarry_691", "Randoms/Looney Larry_691");
        map.put("fran_404", "Animals/Fran_404");
        map.put("squeaks_412", "Animals/Squeaks_412");
        map.put("falcomon_2534", "Digimon/Falcomon_2534");
        map.put("elise_771", "Edgy/Elise_771");
        map.put("saintbernard_2512", "Dogs/Saint Bernard_2512");
        map.put("easterbunny_183", "Holiday/Easter Bunny_183");
        map.put("pilgrimwoman_942", "Holiday/Pilgrim Woman_942");
        map.put("abysinian_2587", "Cats/Abysinian_2587");
        map.put("toonbam_1310", "Oddballs/Toon Bam_1310");
        map.put("masaru_2418", "Digimon/Masaru_2418");
        map.put("todd_1227", "Edgy/Todd_1227");
        map.put("densha_1356", "Randoms/Densha_1356");
        map.put("evilqueen_27096", "Fantasy/Evil Queen_27096");
        map.put("sampson_2535", "Digimon/Sampson_2535");
        map.put("greatdane_2711", "Dogs/Great Dane_2711");
        map.put("bedouinwoman_33246", "Folk/Bedouin Woman_33246");
        map.put("friendlyghost_477", "Monsters/Friendly Ghost_477");
        map.put("diane_55", "Classic/Diane_55");
        map.put("friendlyghost_477", "Randoms/Friendly Ghost_477");
        map.put("birman_2586", "Cats/Birman_2586");
        map.put("lifeguardvince_286", "Beach/Lifeguard Vince_286");
        map.put("cheerful_271", "Smilies/Cheerful_271");
        map.put("donaldtrump_35899", "POLITICS/Donald Trump_35899");
        map.put("ronaldreagan_760", "POLITICS/Ronald Reagan_760");
        map.put("snowman_102", "Holiday/Snowman_102");
        map.put("africanwoman_31258", "Folk/African Woman_31258");
        map.put("capuchinmonkey_1067", "Animals/Capuchin Monkey_1067");
        map.put("astrogeorge_690", "Randoms/AstroGeorge_690");
        map.put("africanman_31238", "Folk/African Man_31238");
        map.put("johnmccain_640", "POLITICS/John McCain_640");
        map.put("crazy_272", "Smilies/Crazy_272");
        map.put("witch_799", "Randoms/Witch_799");
        map.put("ghost_793", "Monsters/Ghost_793");
        map.put("amber_1380", "3D/Amber_1380");
        map.put("dash_406", "Animals/Dash_406");
        map.put("merlin_684", "Toons/Merlin_684");
        map.put("sparky_218", "Dogs/Sparky_218");
        map.put("hiro_214", "Anime/Hiro_214");
        map.put("stephenharper_1043", "POLITICS/Stephen Harper_1043");
        map.put("kai_472", "Anime/Kai_472");
        map.put("dracula_93", "Monsters/Dracula_93");
        map.put("bedouinman_33248", "Folk/Bedouin Man_33248");
        map.put("anna_473", "Anime/Anna_473");
        map.put("jenny_60", "Classic/Jenny_60");
        map.put("joebiden_1469", "POLITICS/Joe Biden_1469");
        map.put("frankenstein_95", "Monsters/Frankenstein_95");
        map.put("teenzack_625", "Toons/Teen Zack_625");
        map.put("akira_1083", "Anime/Akira_1083");
        map.put("mainecoon_2588", "Cats/Mainecoon_2588");
        map.put("apollo_316", "Animals/Apollo_316");
        map.put("jasper_675", "Randoms/Jasper_675");
        map.put("joeybabydoll_491", "Randoms/Joey Babydoll_491");
        map.put("germanman_31365", "Folk/German Man_31365");
        map.put("dana_156", "Classic/Dana_156");
        map.put("kentaro_1064", "Anime/Kentaro_1064");
        map.put("leprechaun_172", "Holiday/Leprechaun_172");
        map.put("scottishfold_2644", "Cats/Scottishfold_2644");
        map.put("siamese_2645", "Cats/Siamese_2645");
        map.put("johnnycorn_890", "Randoms/Johnny Corn_890");
        map.put("chloe_107", "Classic/Chloe_107");
        map.put("beth_2081", "Classic/Beth_2081");
        map.put("billclinton_773", "POLITICS/Bill Clinton_773");
        map.put("ragdoll_2650", "Cats/Ragdoll_2650");
        map.put("alig_2666", "VIP/Ali G_2666");
        map.put("alaskanmalamute_2453", "Dogs/Alaskan Malamute_2453");
        map.put("abelincoln_431", "World/Abe Lincoln_431");
        map.put("doc_274", "Smilies/Doc_274");
        map.put("vladimirputin_806", "POLITICS/Vladimir Putin_806");
        map.put("silvioberlusconi_13824", "POLITICS/Silvio Berlusconi_13824");
        map.put("chester_505", "Animals/Chester_505");
        map.put("indianman_31593", "Folk/Indian Man_31593");
        map.put("rottweiler_2517", "Dogs/Rottweiler_2517");
        map.put("toonpam_1309", "Oddballs/Toon Pam_1309");
        map.put("yoshi_2539", "Digimon/Yoshi_2539");
        map.put("dobermanpinscher_2513", "Dogs/Doberman Pinscher_2513");
        map.put("bat_790", "Animals/Bat_790");
        map.put("hotdog_652", "Randoms/Hotdog_652");
        map.put("cheryl_1196", "Edgy/Cheryl_1196");
        map.put("cockerspaniel_2427", "Dogs/Cocker Spaniel_2427");
        map.put("nari_208", "Anime/Nari_208");
        map.put("scubastephanie_285", "Beach/Scuba Stephanie_285");
        map.put("aneesa_2469", "Classic/Aneesa_2469");
        map.put("mokedog_1045", "Dogs/Moke Dog_1045");
        map.put("mrs.claus_950", "Holiday/Mrs. Claus_950");
        map.put("motokai_1384", "Randoms/MotoKai_1384");
        map.put("mika_216", "Anime/Mika_216");
        map.put("yorkshireterrier_2623", "Dogs/Yorkshire Terrier_2623");
        map.put("abigail_3281", "Classic/Abigail_3281");
        map.put("drummerboy_408", "Holiday/Drummer Boy_408");
        map.put("elf_386", "Holiday/Elf_386");
        map.put("akitainu_2455", "Dogs/Akita Inu_2455");
        map.put("maddiepanda_732", "Animals/Maddie Panda_732");
        map.put("francoishollande_24636", "POLITICS/Francois Hollande_24636");
        map.put("marieantoinette_128", "World/Marie Antoinette_128");
        map.put("angelamerkel_13791", "POLITICS/Angela Merkel_13791");
        map.put("nitro_835", "Edgy/Nitro_835");
        map.put("germanwoman_31576", "Folk/German Woman_31576");
        map.put("ashley_1105", "Classic/Ashley_1105");
        map.put("lalamon_2498", "Digimon/Lalamon_2498");
        map.put("kaya_2402", "Holiday/Kaya_2402");
        map.put("dusty_403", "Dogs/Dusty_403");
        map.put("jesus_434", "World/Jesus_434");
        map.put("bruno_2269", "VIP/Bruno_2269");
        map.put("dai_475", "Anime/Dai_475");
        map.put("japanesewoman_32920", "Folk/Japanese Woman_32920");
        map.put("genie_27304", "Fantasy/Genie_27304");
        map.put("abdul_3370", "Classic/Abdul_3370");
        map.put("teenryan_626", "Toons/Teen Ryan_626");
        map.put("barackobama_2904", "POLITICS/Barack Obama_2904");
        map.put("gingerman_409", "Holiday/Ginger Man_409");
        map.put("jaws_92", "Monsters/Jaws_92");
        map.put("wilbur_405", "Animals/Wilbur_405");
        map.put("farmerjoe_34710", "Randoms/Farmer Joe_34710");
        map.put("bostonterrier_2730", "Dogs/Boston Terrier_2730");
        map.put("kudamon_2538", "Digimon/Kudamon_2538");
        map.put("bones_97", "Monsters/Bones_97");
        map.put("inuitman_32995", "Folk/Inuit Man_32995");
        map.put("karuda_1351", "Randoms/Karuda_1351");
        map.put("beagle_2436", "Dogs/Beagle_2436");
        map.put("tom_77", "3D/Tom_77");
        map.put("teenjake_742", "Toons/Teen Jake_742");
        map.put("wizard_798", "Randoms/Wizard_798");
        map.put("kamemon_2505", "Digimon/Kamemon_2505");
        map.put("hillaryclinton_646", "POLITICS/Hillary Clinton_646");
        map.put("inuitwoman_28548", "Folk/Inuit Woman_28548");
        map.put("robot_482", "Randoms/Robot_482");
        map.put("bassethound_2454", "Dogs/Basset Hound_2454");
        map.put("gwbush_167", "POLITICS/GW Bush_167");
        map.put("pomeranian_2383", "Dogs/Pomeranian_2383");
        map.put("georgewashington_800", "World/George Washington_800");
        map.put("persian_2582", "Cats/Persian_2582");
        map.put("carson_104", "Classic/Carson_104");
        map.put("teenstacey_743", "Toons/Teen Stacey_743");
        map.put("thingersquincy_2025", "Randoms/Thingers Quincy_2025");
        map.put("zombie_99", "Monsters/Zombie_99");
        map.put("cherie_2617", "Toons/Cherie_2617");
        map.put("turkey_110", "Animals/Turkey_110");
        map.put("moses_435", "World/Moses_435");
        map.put("gaomon_2530", "Digimon/Gaomon_2530");
        map.put("teendanielle_749", "Toons/Teen Danielle_749");
        map.put("knight_26877", "Fantasy/Knight_26877");
        map.put("sarahpalin_3461", "POLITICS/Sarah Palin_3461");
        map.put("aiko_217", "Anime/Aiko_217");
        map.put("beachdude_283", "Beach/Beach Dude_283");
        map.put("scrooge_396", "Holiday/Scrooge_396");
        map.put("angela_754", "3D/Angela_754");
        map.put("bumble_892", "Randoms/Bumble_892");
        map.put("japaneseman_32917", "Folk/Japanese Man_32917");
        map.put("yuri_476", "Anime/Yuri_476");
        map.put("teenkevin_736", "Toons/Teen Kevin_736");
        map.put("alien_481", "Randoms/Alien_481");
        map.put("dalmatian_2518", "Dogs/Dalmatian_2518");
        map.put("unicorn_26949", "Fantasy/Unicorn_26949");
        map.put("borat_2251", "VIP/Borat_2251");
        map.put("muggie_2284", "Randoms/Muggie_2284");
        map.put("britneyspears_1115", "VIP/Britney Spears_1115");
        map.put("sweety_275", "Smilies/Sweety_275");
        map.put("thomas_2540", "Digimon/Thomas_2540");
        map.put("beachkid_284", "Beach/Beach Kid_284");
        map.put("chucksockpuppet_731", "Randoms/Chuck Sockpuppet_731");
        map.put("bumble_892", "Animals/Bumble_892");
        map.put("kasumi_215", "Anime/Kasumi_215");
        map.put("koalakiku_934", "Animals/Koala Kiku_934");
        return map;
    }

}
