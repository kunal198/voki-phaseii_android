package com.oddcast.android.voki.httprequests;

import android.content.Context;
import android.util.Log;



import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;


/**
 * Created by Neeraj on 09-Jul-15.
 */
public class Connector {
Context context;



    HttpRequest request;


    /***
     * method for post data to server..
     * @param list
     * @param Url
     * @return
     */
    public String getStringData(HashMap<String,String> list, String Url, String method){
        try {
            request = new HttpRequest(Url);
            return request.prepare(method).sendAndReadString(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * method for delete data from server...
     * @param list
     * @param url
     * @return
     */

    public JSONObject deleteData(HashMap<String,String> list,String url){

        try{
            request = new HttpRequest(url);
            return  request.deletePost().sendAndReadJSON(false);
        }catch (Exception e){
            e.printStackTrace();
        }

        return  null;

    }

    /**
     * Method for convert input response into JSON..
     * @param is
     * @return
     */
    public JSONObject convertResponse(InputStream is){
        JSONObject jParentObject = null;
    try {

        Log.e("INSIDE CONVERSION", "RESPONSE:" +is.toString());
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }

        is.close();

        String result = sb.toString();
        Log.e("INSIDE CONVERSION", "RESPONSE:" +result);
        jParentObject = new JSONObject(result);
        return  jParentObject;
    }catch (Exception e){
        e.printStackTrace();
        return  jParentObject;
    }

    }




}
