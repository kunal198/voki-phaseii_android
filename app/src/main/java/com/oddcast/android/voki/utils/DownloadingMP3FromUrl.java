package com.oddcast.android.voki.utils;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.custom_listeners.StoppingMediaRecorder;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by brst-pc20 on 12/16/16.
 */

public class DownloadingMP3FromUrl extends AsyncTask<String, Integer, String> {
    String mp3_url = "";
    String audioId="";
    private String curFX;
    StoppingMediaRecorder stoppingMediaRecorder;
    public DownloadingMP3FromUrl(String mp3_url,StoppingMediaRecorder stoppingMediaRecorder) {
        this.mp3_url = mp3_url;

        this.stoppingMediaRecorder=stoppingMediaRecorder;
        Log.d("mp3_url", "" + mp3_url);
        Log.d("audioId", "" + audioId);
        Log.d("curFX", "" + curFX);
    }

    @Override
    protected String doInBackground(String... url) {
        int count;
        try {
            URL url1 = new URL(mp3_url);
            URLConnection conexion = url1.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            InputStream input = new BufferedInputStream(url1.openStream());
            OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.EFFECTS_AUDIO_PATH);
            byte data[] = new byte[1024];
            long total = 0;
            System.out.println("downloading.............");
            Log.d("log_tagg","downloading..........");
            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress((int) ((total / (float) lenghtOfFile) * 100));
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            if (MainActivity.progress!=null)
            MainActivity.progress.dismiss();
            Log.d("log_tagg","downloading.........."+e);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
   /*     String buttonPressed = "1";
        String acc_id = "37533";
        String url = "curFX=" + curFX + "&audioId=" + audioId + "&buttonPressed=" + buttonPressed + "&acc_id=" + acc_id;
        String urlEncoded = "http://vhss.oddcast.com/admin/saveFXAudio.php?" + url;
        Log.d("urlencoded",""+urlEncoded);*/
        Log.d("log_tagg","stoppingMediaRecorder.");
        if (stoppingMediaRecorder!=null)
            stoppingMediaRecorder.playAudio();



    }
}
