package com.oddcast.android.voki.PopUp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.IonListners;

/**
 * Created by brst-pc81 on 7/7/16.
 */
public class DeletePopUp extends Dialog implements View.OnClickListener {

    private Context mContext;
    private ImageView ivCancel;
    private LinearLayout llYes, llNo;
    private IonListners iListners;
    private int position;
    boolean savecharcase;
    TextView messageTV;

    public DeletePopUp(Context context, IonListners iListners, int position,boolean savecharcase) {
        super(context);
        this.mContext = context;
        this.iListners = iListners;
        this.position = position;
        this.savecharcase=savecharcase;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.pop_menu);
        ivCancel = (ImageView) findViewById(R.id.iv_cancel);
        llNo = (LinearLayout) findViewById(R.id.ll_no);
        llYes = (LinearLayout) findViewById(R.id.ll_yes);
        messageTV= (TextView) findViewById(R.id.messageTV);

        ivCancel.setOnClickListener(this);
        if (savecharcase)
        {

            llNo.setOnClickListener(this);
            llYes.setOnClickListener(this);
        }
        else{
            llNo.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
            messageTV.setText("Internet connection required");
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_no:
                dismiss();
                break;

            case R.id.ll_yes:
                if (iListners != null) {
                    iListners.removeSaveCharcter(position);
                }
                dismiss();
                break;
            case R.id.iv_cancel:
                dismiss();
                break;

            default:
                break;
        }

    }
}
