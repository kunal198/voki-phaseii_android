package com.oddcast.android.voki.PopUp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.oddcast.android.voki.MySharedPreferences;
import com.oddcast.android.voki.R;

/**
 * Created by brst-pc20 on 8/20/16.
 */
public class ReviewPopUp extends Dialog implements View.OnClickListener {

    private Context mContext;

    private TextView reviewTV, remindLaterTV, dontAskAgainTV;

    public ReviewPopUp(Context context) {
        super(context);
        this.mContext = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.review_popup);

        reviewTV = (TextView) findViewById(R.id.reviewTV);
        remindLaterTV = (TextView) findViewById(R.id.remindLaterTV);
        dontAskAgainTV = (TextView) findViewById(R.id.dontAskAgainTV);
        reviewTV.setOnClickListener(this);
        remindLaterTV.setOnClickListener(this);
        dontAskAgainTV.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.reviewTV:
              // package name of the app
                MySharedPreferences.getInstance().setIsNeedToProcessReviewPopup(mContext,false);
                try {
                    mContext. startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.oddcast.android.voki")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    anfe.printStackTrace();
                }
                dismiss();
                break;
            case R.id.remindLaterTV:
                int eligible_count=MySharedPreferences.getInstance().getEligibleCount(mContext);
                eligible_count=eligible_count+3;
                MySharedPreferences.getInstance().setEligibleCount(mContext,eligible_count);
                MySharedPreferences.getInstance().saveSharingCount(mContext,0);
                dismiss();
                break;
            case R.id.dontAskAgainTV:
                MySharedPreferences.getInstance().setIsNeedToProcessReviewPopup(mContext,false);
                MySharedPreferences.getInstance().setEligibleCount(mContext,-1);
                dismiss();
                break;

            default:
                break;
        }

    }
}
