package com.oddcast.android.voki.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.oddcast.android.voki.Modal.CharLanguageModel;
import com.oddcast.android.voki.Modal.LanguageModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc16 on 5/2/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;

    // Database Name ...
    private static final String DATABASE_NAME = "saved character";

    // Table Name ..
    public static final String CHARACTERS_DATA = "characters_saved";
    public static final String LANGUAGE_TABLE = "language_table";
    public static final String CHAR_LANGUAGE_TABLE = "char_language_table";


    /**
     * CHARACTERS_DATA Table Column Names ....
     */
    public static final String COLUMN_ID = "id";
    public static final String CHARACTER_NAME = "char_name";
    public static final String CHARACTER_CATEGORY = "char_cat";
    public static final String CHARACTER_BACKGROUND = "char_back";
    public static final String CHAR_X_POSITION = "char_x_pos";
    public static final String CHAR_Y_POSITION = "char_y_pos";
    public static final String CHAR_IMAGE_ICON = "char_image_icon";
    public static final String CHAR_DEFAULT_POS = "char_default_position";

    public static final String CHAR_DATA = "cahr_data";
    public static final String CHAR_IMG = "char_image";


    public static final String TIMESTAMP = "timestamp";


    public static final String EYES_COLOR = "eye_color";
    public static final String MOUTHCOLOR = "mouth_color";
    public static final String HAIRCOLOR = "hair_color";
    public static final String SKINCOLOR = "skincolor";
    public static final String CHARACTER_URL = "character_url";
    /**
     * Language Table Column Names ....
     */
//    public static final String COLUMN_ID = "id";
    public static final String LANGUAGE_NAME = "language_name";
    public static final String LANGUAGE_ID = "language_id";

    /**
     * Char Language Info Table Column Names ....
     */
//    public static final String COLUMN_ID = "id";
//    public static final String CHARACTER_NAME = "char_name";
//    public static final String LANGUAGE_ID = "language_id";
    public static final String VOICE_ID = "voice_id";
    public static final String ENGINE_ID = "engine_id";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHARACTER_TABLE = "CREATE TABLE " + CHARACTERS_DATA + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + CHAR_DATA + " TEXT, " + CHAR_IMG + " TEXT)";



        String CREATE_LANGUAGE_TABLE = "CREATE TABLE " + LANGUAGE_TABLE + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + LANGUAGE_NAME + " TEXT, " +
                LANGUAGE_ID + " TEXT)";

        String CREATE_CHAR_LANG_TABLE = "CREATE TABLE " + CHAR_LANGUAGE_TABLE + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + LANGUAGE_ID + " TEXT, " + CHARACTER_NAME + " TEXT, " +
                VOICE_ID + " TEXT, " + ENGINE_ID + " TEXT)";

        Log.e("CREATE_CHARACTER_TABLE", CREATE_CHARACTER_TABLE);
        db.execSQL(CREATE_CHARACTER_TABLE);
        db.execSQL(CREATE_LANGUAGE_TABLE);
        db.execSQL(CREATE_CHAR_LANG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS characters_saved");
        db.execSQL("DROP TABLE IF EXISTS language_table");
        db.execSQL("DROP TABLE IF EXISTS char_language_table");
        onCreate(db);
    }

    /**************************************************************************************************************************/

    /**
     * Insert Character ....
     *
     * @return
     */
    public boolean insertCharacter(String json, String img) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(CHAR_DATA, json);
        cv.put(CHAR_IMG, img);

        long id = db.insert(CHARACTERS_DATA, null, cv);
        db.close();

        if (id != -1) {
            Log.d("character_inserted", "Inserted Successfully");
            return true;
        } else {
            return false;
        }
    }

    /**
     * delete char
     *
     * @param charId
     * @return
     */

    public void deletePost(String charId) {
        String pos = "" + charId;
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause;
        whereClause = COLUMN_ID + "='" + pos + "'";
        String query = "delete from " + CHARACTERS_DATA + " where " + whereClause;
        Log.e("queryquery", "" + query);
        db.execSQL(query);
        db.close();
    }

    /**
     * Insert Languages ......
     *
     * @param languageName
     * @param languageId
     * @return
     */
    public boolean insertLanguage(String languageName, String languageId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LANGUAGE_NAME, languageName);
        cv.put(LANGUAGE_ID, languageId);


        long id = db.insert(LANGUAGE_TABLE, null, cv);
        db.close();

        if (id != -1) {
            Log.d("language_inserted", "Inserted Successfully");
            return true;
        } else {
            return false;
        }
//        return true;
    }

    /**
     * Insert Character Language ......
     *
     * @param langId
     * @param charName
     * @param voiceId
     * @param engineId
     * @return
     */
    public boolean insertCharLanguage(String langId, String charName, String voiceId, String engineId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LANGUAGE_ID, langId);
        cv.put(CHARACTER_NAME, charName);
        cv.put(VOICE_ID, voiceId);
        cv.put(ENGINE_ID, engineId);


        long id = db.insert(CHAR_LANGUAGE_TABLE, null, cv);
        db.close();

        if (id != -1) {
            Log.d("language_inserted", "Inserted Successfully");
            return true;
        } else {
            return false;
        }
//        return true;
    }

    /********************************************************************************************************************************************/

    /**
     * Method to get ALl Saved Characters ....
     *
     * @return
     */
    public ArrayList<HashMap<String, String>> getAllSavedCharacters() {
        ArrayList<HashMap<String, String>> bList = new ArrayList<>();

        // Select All Query ...
        String selectQuery = "SELECT * FROM " + CHARACTERS_DATA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<>();

                map.put(CHAR_IMG, cursor.getString(cursor.getColumnIndex(CHAR_IMG)));
                map.put(CHAR_DATA, cursor.getString(cursor.getColumnIndex(CHAR_DATA)));

                bList.add(map);

                Log.d("fetch_data", "" + bList);
            } while (cursor.moveToNext());
        }
        // return data list ...
        db.close();
        return bList;
    }

    /**
     * fetchid for remove values in local database
     * @return
     */
    public ArrayList<String> fetchId() {
        ArrayList<String> bList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + CHARACTERS_DATA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String id;
                id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
                bList.add(id);

            } while (cursor.moveToNext());
        }
        // return data list ...
        db.close();
        return bList;
    }

    /**
     * Method for getting All Languages ....
     *
     * @return
     */
    public ArrayList<LanguageModel> getAllSavedLanguages() {
        ArrayList<LanguageModel> bList = new ArrayList<>();

        // Select All Query ...
        String selectQuery = "SELECT * FROM " + LANGUAGE_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                LanguageModel langModel = new LanguageModel();

                langModel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
                langModel.setLanguageId(cursor.getString(cursor.getColumnIndex(LANGUAGE_ID)));

                // Adding Data to List ...
                bList.add(langModel);

                Log.d("fetch_data", "" + bList);
            } while (cursor.moveToNext());
        }
        // return data list ...
        db.close();
        return bList;
    }

    public ArrayList<CharLanguageModel> getAllLanguages() {
        ArrayList<CharLanguageModel> bList = new ArrayList<>();

        // Select All Query ...
        String selectQuery = "SELECT * FROM " + CHAR_LANGUAGE_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CharLanguageModel langModel = new CharLanguageModel();

                langModel.setLangId(cursor.getString(cursor.getColumnIndex(LANGUAGE_ID)));
                langModel.setCharName(cursor.getString(cursor.getColumnIndex(CHARACTER_NAME)));
                langModel.setVoiceId(cursor.getString(cursor.getColumnIndex(VOICE_ID)));
                langModel.setEngineId(cursor.getString(cursor.getColumnIndex(ENGINE_ID)));


                // Adding Data to List ...
                bList.add(langModel);

                Log.d("fetch_data", "" + bList);
            } while (cursor.moveToNext());
        }
        // return data list ...
        db.close();
        return bList;
    }


    /**
     * Get ALL char languages ....
     *
     * @return
     */
    public ArrayList<CharLanguageModel> getAllCharLanguages(String langId) {
        ArrayList<CharLanguageModel> bList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM char_language_table WHERE language_id=" + langId;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    CharLanguageModel langModel = new CharLanguageModel();

                    langModel.setLangId(cursor.getString(cursor.getColumnIndex(LANGUAGE_ID)));
                    langModel.setCharName(cursor.getString(cursor.getColumnIndex(CHARACTER_NAME)));
                    langModel.setVoiceId(cursor.getString(cursor.getColumnIndex(VOICE_ID)));
                    langModel.setEngineId(cursor.getString(cursor.getColumnIndex(ENGINE_ID)));

                    // Adding Data to List ...
                    bList.add(langModel);

                    Log.d("fetch_data", "" + bList);
                } while (cursor.moveToNext());
            }
        }
        db.close();
        return bList;


    }
}
