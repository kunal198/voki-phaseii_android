package com.oddcast.android.voki.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oddcast.android.voki.MainActivity;
import com.oddcast.android.voki.Modal.ShowCategoryModel;
import com.oddcast.android.voki.R;
import com.oddcast.android.voki.custom_listeners.CharacterListener;
import com.oddcast.android.voki.utils.Constants;


import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;
import java.util.WeakHashMap;

/**
 * Created by brst-pc16 on 3/30/16.
 */
public class ListCharacterAdapter extends BaseAdapter {
    Context mContext;
    Integer[] images;
    ArrayList<ShowCategoryModel> charactersCategory;
    int newPosition = -1;
    String selectedCategory = "";
    int select_pos;
    int new_pos;
    CharacterListener listener;
    boolean isSame = false;
    ImageLoader imageLoader = ImageLoader.getInstance();
    boolean isSelect = false;
    private boolean isRandom = false;

    private WeakHashMap<Integer, WeakReference<View>> hashMap = new WeakHashMap<Integer, WeakReference<View>>();


    public ListCharacterAdapter(Context mContext, ArrayList<ShowCategoryModel> charactersCategory, CharacterListener listener, boolean isRandom) {
        this.mContext = mContext;
        this.charactersCategory = charactersCategory;
        this.isRandom = isRandom;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return charactersCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return charactersCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_layout_characters, null, true);
        }

        hashMap.put(position, new WeakReference<View>(convertView));

        ImageView im_characters = (ImageView) convertView.findViewById(R.id.im_character);
        ShowCategoryModel model = charactersCategory.get(position);
        try {
            if (model.getSelection().equals("true")) {

                String folderName = model.getFileName();

                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/icons/over/btn.png";
                imageLoader.displayImage(uri, im_characters);

            } else {
                String folderName = model.getFileName();
                String uri = "file:///mnt/sdcard/.VokiData/assets/img/char/" + folderName + "/icons/base/btn.png";
                Log.e("imgUri", "" + uri);
                imageLoader.displayImage(uri, im_characters);

            }
            if (isRandom) {
                Random random = new Random();
                showList(random.nextInt(position), false);
            }
            im_characters.setTag(position);

            im_characters.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    System.out.println(" position " + pos);

                    MainActivity.im_library.setImageResource(R.drawable.btn_library_unselect);
                    Log.e("chraccat", "called" );
                    select_pos = pos;
                    showList(select_pos, false);
                }
            });

            im_characters.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.e("chraccat", "called" );
                    String file = charactersCategory.get(position).getFileName();
                    Toast.makeText(mContext, file, Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void showList(final int pos, boolean istrue) {
        if (MainActivity.ivTrash.getVisibility() == View.VISIBLE) {
            MainActivity.ivTrash.setVisibility(View.GONE);
        }
        isSelect = istrue;
        System.out.print("Test " + pos);
        new_pos = pos;
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ShowCategoryModel mod;
                if (newPosition == -1) {

                    for (int i = 0; i < charactersCategory.size(); i++) {
                        if (i == new_pos) {
                            mod = charactersCategory.get(new_pos);
                            mod.setSelection("true");
                            charactersCategory.remove(new_pos);
                            charactersCategory.add(new_pos, mod);
                            Log.e("inforIf", "" + new_pos + "--" + i);
                        } else {
                            mod = charactersCategory.get(i);
                            mod.setSelection("false");
                            charactersCategory.remove(i);
                            charactersCategory.add(i, mod);
                            Log.e("inforif", "" + new_pos + "--" + i);
                        }
                    }


                    if (new_pos != 1) {
                        mod = charactersCategory.get(1);
                        mod.setSelection("false");
                        charactersCategory.remove(1);
                        charactersCategory.add(1, mod);

                    }
                } else {


                    for (int i = 0; i < charactersCategory.size(); i++) {
                        if (i == new_pos) {
                            mod = charactersCategory.get(new_pos);
                            mod.setSelection("true");
                            charactersCategory.remove(new_pos);
                            charactersCategory.add(new_pos, mod);
                            Log.e("inforIf", "" + new_pos + "--" + i);
                        } else {
                            mod = charactersCategory.get(i);
                            mod.setSelection("false");
                            charactersCategory.remove(i);
                            charactersCategory.add(i, mod);
                            Log.e("inforif", "" + new_pos + "--" + i);
                        }
                    }


                    Log.e("check_positions", "" + new_pos + "--" + newPosition);
                    if (new_pos != newPosition) {
                        mod = charactersCategory.get(newPosition);
                        mod.setSelection("false");
                        charactersCategory.remove(newPosition);
                        charactersCategory.add(newPosition, mod);

                    }

                }

                notifyDataSetChanged();

                Constants.NEW_CHARACTER_CATEGORY = charactersCategory.get(new_pos).getFileName();


                if (!isSelect) {
                    if (new_pos == newPosition) {
                        selectedCategory = charactersCategory.get(new_pos).getFileName();

                        if (isSame) {
                            isSame = false;
                        } else {
                            isSame = true;
                        }

                        if (listener != null) {
                            listener.updateCharaterList(selectedCategory, isSame, false,pos);
                            Log.e("selectedCategory", "Inside");
                        }
                    } else {
                        selectedCategory = charactersCategory.get(new_pos).getFileName();

                        Log.e("selectedCategory", "" + selectedCategory);
                        isSame = false;

                        if (listener != null) {
                            Log.e("selectedCategory", "Inside1");
                            listener.updateCharaterList(selectedCategory, isSame, false,pos);
                        }
                    }
                    MainActivity.ISANIM = true;
                } else {
                    selectedCategory = charactersCategory.get(new_pos).getFileName();
                    isSame = false;
                    Log.e("inListner", "" + isSame);
                    if (listener != null) {
                        Log.e("selectedCategory", "Inside2"+selectedCategory+"same"+isSame);
                        listener.updateCharaterList(selectedCategory, isSame, true,pos);

                    }
                    MainActivity.ISANIM = false;
                }


                newPosition = new_pos;


            }
        });

    }
}
